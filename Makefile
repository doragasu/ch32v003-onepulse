CH32V003FUN:=$(HOME)/src/github/ch32v003fun/ch32v003fun
TARGET:=onepulse

all : flash

test : flash
	$(CH32V003FUN)/../minichlink/minichlink -X ECLK:1:0:0:1:63

include $(CH32V003FUN)/ch32v003fun.mk

flash : cv_flash
clean : cv_clean

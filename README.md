# Introduction

This is a toy project to test the RISC-V powered "10-cent MCU" CH32V003 chips. I needed to generate a 200 ms low level pulse each time a switch was closed, and decided this was the perfect project to test these chips. To build it I used the minimalistic [ch32v003fun environment](https://github.com/cnlohr/ch32v003fun), that I find quite awesome for small projects.

# Building

Follow [ch32v003fun installation instructions](https://github.com/cnlohr/ch32v003fun/wiki/Installation). When done, edit the `Makefile` to point the `CH32V003FUN` variable to your cs32v003fun installation path and just run `make`. This should build the project and flash the binary to the MCU (if you have the device and programmer plugged).

# Usage

The pulse is generated on D6, and triggers on low level transitions of C5. Only one trigger is generated each 10 seconds at most.

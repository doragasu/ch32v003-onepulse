/*
 * Generate a single 200 ms pulse on switch activation. Once switch is
 * activated, it cannot be activated again in 10 seconds.
 *
 * WIRING:
 * - Output pin on D6
 * - Input switch on C5 (must close to ground)
 */

#include <ch32v003fun.h>

#define SWITCH_READ() (GPIOC->INDR & (1<<5))

int main(void)
{
	SystemInit();

	// Enable GPIOs
	RCC->APB2PCENR = RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD;

	// GPIO C5 for input pin change, D6 for LED
	GPIOD->CFGLR =
		((GPIO_CNF_IN_PUPD)<<(4*1)) | // Keep SWIO enabled.
		((GPIO_Speed_2MHz | GPIO_CNF_OUT_PP)<<(4*6)); // D6 = GPIOD OUT
	GPIOC->CFGLR = (GPIO_Speed_In | GPIO_CNF_IN_PUPD)<<(4*5); // C5 = GPIOD IN
	GPIOC->OUTDR = 1<<5; // Pull up on C5
	GPIOD->BSHR = 1<<6;  // Output high by default

	while(1) {
		while (0 == SWITCH_READ());
		Delay_Ms(500);	// Debounce transition to 1
		if (0 == SWITCH_READ()) {
			// 200 ms pulse on D6
			GPIOD->BCR = 1<<6;
			Delay_Ms(200);
			GPIOD->BSHR = 1<<6;
			// Do not allow activation for 10 seconds
			Delay_Ms(9300);
		}
	}
}
